import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";

import Home from "./components/home/Home";
import ProductList from "./components/product/ProductList";
import ViewProduct from "./components/product/ViewProduct";
import UserLayout from "./components/layouts/UserLayout";
import ProductCategoryList from "./components/product/ProductCategoryList";
import SearchedProducts from "./components/product/SearchedProducts";
import CategoryList from "./components/categories/CategoryList";
import CategoryProductList from "./components/categories/CategoryProductList";
import ViewCatagoryProduct from "./components/categories/ViewCatagoryProduct";
import AboutUs from "./components/aboutUs/AboutUs";
import ContactUs from "./components/contactUs/ContactUs";
import Profile from "./components/profile/Profile";
import Login from "./components/auth/Login";

const App = () => {
  return (
    <Routes>
      <Route path="/" element={<UserLayout />}>
        <Route index element={<Home />} />

        <Route path="/auth">
          <Route index element={<Navigate to="login" replace />} />
          <Route path="login" element={<Login />} />
        </Route>

        <Route path="/products">
          <Route index element={<ProductList />} />

          <Route path="search" element={<SearchedProducts />} />

          <Route path=":slug" element={<ViewProduct />} />
          <Route path="catalog/:category" element={<ProductCategoryList />} />
        </Route>

        <Route path="/categories">
          <Route index element={<CategoryList />} />
          <Route path=":category">
            <Route index element={<CategoryProductList />} />
            <Route path=":slug" element={<ViewCatagoryProduct />} />
          </Route>
        </Route>

        <Route path="/about">
          <Route index element={<AboutUs />} />
        </Route>
        <Route path="/contact">
          <Route index element={<ContactUs />} />
        </Route>
        <Route path="/profile">
          <Route index element={<Profile />} />
        </Route>
      </Route>
    </Routes>
  );
};

export default App;
