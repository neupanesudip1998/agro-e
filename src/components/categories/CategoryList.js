import React, { useEffect, useState } from "react";
import APIServices from "../../httpServices/httpServices";
import Loading from "../common/Loading";
import SingleCategory from "../common/cards/category/SingleCategory";
import { LIMIT } from "../../utils/constant";

const CategoryList = () => {
  const [categories, setCategories] = useState([]);
  const [loading, setLoading] = useState(true);

  const getCategories = async () => {
    const { data, success } = await new APIServices("category/list").post({
      limit: LIMIT,
    });

    if (success) {
      setCategories(data);
      setLoading(false);
    }
  };

  useEffect(() => {
    getCategories();
  }, []);
  return (
    <>
      {loading ? (
        <div className="h-96 flex items-center bg-slate-100">
          <div className="relative left-1/2">
            <Loading />
          </div>
        </div>
      ) : (
        <div className="p-0 flex flex-wrap max-[500px]:justify-between">
          {categories.length > 0 &&
            categories.map((category, idx) => (
              <SingleCategory key={idx} category={category} />
            ))}
        </div>
      )}
    </>
  );
};

export default CategoryList;
