import React, { useEffect, useState } from "react";
import { useParams, useSearchParams } from "react-router-dom";
import APIServices from "../../httpServices/httpServices";
import Loading from "../common/Loading";
import ProductCard from "../common/cards/product/ProductCard";
import { LIMIT } from "../../utils/constant";

const CategoryProductList = () => {
  const { category } = useParams();

  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const getProducts = async () => {
    const { data, success } = await new APIServices(
      `category/products/${category}`
    ).post({
      limit: LIMIT,
    });

    if (success) {
      setProducts(data?.products);

      setLoading(false);
    }
  };

  useEffect(() => {
    getProducts();
  }, []);

  return (
    <>
      {loading ? (
        <div className="h-96 flex items-center bg-slate-100">
          <div className="relative left-1/2">
            <Loading />
          </div>
        </div>
      ) : (
        <div>
          <div></div>
          <div className="p-0 flex flex-wrap max-[500px]:justify-between">
            {products.length > 0 &&
              products.map((product, idx) => (
                <div key={idx} className="mx-1 md:mx-0">
                  <ProductCard product={product} />
                </div>
              ))}
          </div>
        </div>
      )}
    </>
  );
};

export default CategoryProductList;
