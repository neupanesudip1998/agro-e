import React from "react";

const Arrow = ({ handleClick, arrow }) => {
  return (
    <div
      onClick={handleClick}
      className={` ${
        arrow === "left" && "rotate-180 right-0"
      } absolute cursor-pointer mx-2 bg-gradient-to-r from-gray-900 top-1/2 transform -translate-y-1/2 hover:from-gray-600 hover:to-transparent rounded-full`}
    >
      <svg
        className="w-12 md:w-20"
        viewBox="-2.4 -2.4 28.80 28.80"
        xmlns="http://www.w3.org/2000/svg"
        fill="#000000"
        stroke="#000000"
        transform="matrix(-1, 0, 0, 1, 0, 0)"
      >
        <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
        <g
          id="SVGRepo_tracerCarrier"
          strokeLinecap="round"
          strokeLinejoin="round"
          stroke="#CCCCCC"
          strokeWidth="0.288"
        ></g>
        <g id="SVGRepo_iconCarrier">
          <title></title>
          <g id="Complete">
            <g id="arrow-right">
              <g>
                <polyline
                  data-name="Right"
                  fill="none"
                  id="Right-2"
                  points="16.4 7 21.5 12 16.4 17"
                  stroke="#ffffff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="1.8960000000000001"
                ></polyline>

                <line
                  fill="none"
                  stroke="#ffffff"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="1.8960000000000001"
                  x1="2.5"
                  x2="19.2"
                  y1="12"
                  y2="12"
                ></line>
              </g>
            </g>
          </g>
        </g>
      </svg>
    </div>
  );
};

export default Arrow;
