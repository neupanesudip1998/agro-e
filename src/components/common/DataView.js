import React from "react";

const DataView = ({ label, value }) => {
  return (
    <p className="text-base leading-4 mt-7 text-gray-600">
      <span className="font-semibold">{label}</span>
      {` ${value}`}
    </p>
  );
};

export default DataView;
