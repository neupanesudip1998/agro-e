import React, { useEffect, useState } from "react";
import { useNavigate, useSearchParams } from "react-router-dom";

const Filter = ({
  getProducts,
  categories,
  filterObj,
  setFilterObj,
  setLoading,
  initial,
}) => {
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const searchTxt = searchParams.get("q");

  let currentUrl = `/products/search?${
    searchTxt ? "q=" + searchTxt + "&" : ""
  }${"page="}`;

  const [change, setChange] = useState(false);
  const [model, setModel] = useState(false);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFilterObj({ ...filterObj, [name]: value });
    if (name === "OnSell") {
      setChange(true);
      navigate(`${currentUrl}1`);
    } else {
      setChange(false);
    }
  };

  const handleCategory = (e) => {
    const { name, value } = e.target;
    const categories = filterObj.Categories;
    let newCat = [];
    if (categories.includes(value)) {
      newCat = categories?.filter((item) => item !== value);
      setFilterObj({
        ...filterObj,
        [name]: [...newCat],
      });
    } else {
      newCat = [...categories, value];
      setFilterObj({ ...filterObj, [name]: [...newCat] });
    }
    setChange(true);
    navigate(`${currentUrl}1`);
  };

  useEffect(() => {
    if (change) {
      setLoading(true);
      getProducts();
    }
  }, [filterObj, change]);

  return (
    <div>
      <div className="flex justify-between  ">
        <p className="text-lg font-semibold">Filter</p>
        <span onClick={() => setModel((prev) => !prev)} className="md:hidden">
          expand
        </span>
      </div>

      <div
        className={`${
          !model && "hidden"
        } bg-gray-100 p-2 z-10 text-xs absolute md:relative md:text-base w-full  grid grid-cols-3 grid-gap-2 md:block md:w-48`}
      >
        <div className="">
          <p className="font-semibold">Categories</p>

          {categories &&
            categories?.map((category, idx) => (
              <div key={idx}>
                <input
                  type="checkbox"
                  name="Categories"
                  value={category?._id}
                  checked={filterObj?.Categories?.includes(category?._id)}
                  onChange={(e) => handleCategory(e)}
                />
                <label className="ml-2">{category?.Name}</label>
              </div>
            ))}
        </div>

        <div className="">
          <p className="font-semibold">On Sell</p>
          <input
            type="radio"
            name="OnSell"
            value="Yes"
            checked={filterObj?.OnSell === "Yes"}
            onChange={(e) => handleChange(e)}
          />
          <label className="mr-4">Yes</label>

          <input
            type="radio"
            name="OnSell"
            value="No"
            checked={filterObj?.OnSell === "No"}
            onChange={(e) => handleChange(e)}
          />
          <label>No</label>
        </div>

        <div className="">
          <div className="flex justify-between items-center ">
            <p className="font-semibold">Price</p>
            <button
              onClick={() => {
                if (!!filterObj.MinPrice || !!filterObj.MaxPrice) {
                  setChange(true);
                }
              }}
              className="bg-yellow-400 px-1 rounded text-white"
            >
              Go
            </button>
          </div>
          <input
            className="w-full border"
            type="number"
            name="MinPrice"
            placeholder="From"
            value={filterObj.MinPrice}
            onChange={(e) => handleChange(e)}
          />
          <br />
          <input
            className="w-full border"
            type="number"
            name="MaxPrice"
            placeholder="To"
            value={filterObj.MaxPrice}
            onChange={(e) => handleChange(e)}
          />
        </div>
      </div>

      <button
        onClick={() => {
          setFilterObj(initial);
          setChange(true);
        }}
        className=" w-full bg-red-500 text-white rounded my-1"
      >
        Reset
      </button>
    </div>
  );
};

export default Filter;
