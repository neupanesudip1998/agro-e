import React, { useEffect, useRef, useState } from "react";
import {
  Link,
  useLocation,
  useNavigate,
  useSearchParams,
} from "react-router-dom";

const Pagination = ({ totalPagesNumber, currentPage }) => {
  const ref = useRef(null);
  const navigate = useNavigate();
  const [searchParams, setSearchParams] = useSearchParams();
  const [view, setView] = useState([]);

  const searchTxt = searchParams.get("q");
  const { pathname } = useLocation();

  let currentUrl = `${pathname}?${
    searchTxt ? "q=" + searchTxt + "&" : ""
  }${"page="}`;

  const buttonClasses =
    "relative block text-white px-1 transition-all duration-200  hover:bg-blue-800 font-semibold dark:hover:bg-gray-700 rounded-md";

  const hello = () => {
    let arr = [];

    for (let i = 1; i <= totalPagesNumber; i++) {
      arr.push({
        label: i,
        value: currentUrl + i,
      });
    }
    setView(arr);
  };

  useEffect(() => {
    hello();
  }, []);

  return (
    <div className="flex justify-between items-center my-4 mx-2 max-md:text-xs">
      <div className="flex items-center">
        <button
          className={`${buttonClasses} ${
            currentPage === 1
              ? "pointer-events-none bg-blue-200"
              : "bg-blue-600 rotate-180"
          }`}
          onClick={() => navigate(`${currentUrl}${currentPage - 1}`)}
          disabled={currentPage === 1}
        >
          <SVG />
        </button>
        <div
          ref={ref}
          onScroll={(e) => {
            if (
              e.target.scrollLeft + e.target.clientWidth + 1 >
              e.target.scrollWidth
            ) {
              ref.current.scrollLeft -= e.target.scrollWidth;
            }
          }}
          className="flex justify-between items-center max-w-[10.5rem]   overflow-x-scroll scroll-smooth hello-hello"
        >
          {view.map((item, idx) => (
            <Link
              key={idx}
              to={item.value}
              className="py-1 px-2 bg-blue-300 mx-1 rounded-md"
            >
              {item.label}
            </Link>
          ))}
        </div>
        <button
          className={`${buttonClasses} ${
            currentPage === totalPagesNumber
              ? "pointer-events-none bg-blue-200"
              : "bg-blue-600"
          }`}
          onClick={() => navigate(`${currentUrl}${currentPage + 1}`)}
          disabled={currentPage === totalPagesNumber}
        >
          <SVG />
        </button>
      </div>
    </div>
  );
};

export default Pagination;

export const SVG = () => {
  return (
    <svg
      width="20px"
      height="25px"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      fill="#000000"
    >
      <g id="SVGRepo_bgCarrier" strokeWidth="0"></g>
      <g
        id="SVGRepo_tracerCarrier"
        strokeLinecap="round"
        strokeLinejoin="round"
      ></g>
      <g id="SVGRepo_iconCarrier">
        <title></title>
        <g id="Complete">
          <g id="arrow-right">
            <g>
              <polyline
                data-name="Right"
                fill="none"
                id="Right-2"
                points="16.4 7 21.5 12 16.4 17"
                stroke="#ffffff"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2.5"
              ></polyline>
              <line
                fill="none"
                stroke="#ffffff"
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2.5"
                x1="2.5"
                x2="19.2"
                y1="12"
                y2="12"
              ></line>
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};
