import React from "react";

const ViewImage = ({ source, ...props }) => {
  return (
    <img
      src={process.env.REACT_APP_IMAGE_BASE_URL + source}
      alt=""
      {...props}
    />
  );
};

export default ViewImage;
