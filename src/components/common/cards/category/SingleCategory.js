import React from "react";
import ViewImage from "../../ViewImage";
import { Link } from "react-router-dom";

const SingleCategory = ({ category }) => {
  return (
    <div className="flex-shrink-0 mx-0 md:mx-2 mb-5 inline-block max-w-sm  w-40 sm:w-36 md:w-72 h-76 bg-white border border-gray-200 rounded-b-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <div className="w-full h-28 md:w-72 md:h-56">
        <ViewImage
          source={category?.Image}
          alt={category?.Name}
          className="w-full h-28 md:w-72 md:h-56"
        />
      </div>

      <div className="px-2 md:px-5 py-1">
        <Link to={`/categories/${category?.Slug}`}>
          <h5 className="text-md md:text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
            {category?.Name}
          </h5>
        </Link>
      </div>
    </div>
  );
};

export default SingleCategory;
