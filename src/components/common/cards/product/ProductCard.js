import React from "react";
import ViewImage from "../../ViewImage";
import { Link } from "react-router-dom";

const ProductCard = ({ product }) => {
  return (
    <div className="flex-shrink-0 mx-0 md:mx-2 mb-5 inline-block max-w-sm  w-40 sm:w-36 md:w-72 h-76 bg-white border border-gray-200 rounded-b-lg shadow dark:bg-gray-800 dark:border-gray-700">
      <div className="w-full h-28 md:w-72 md:h-56">
        <ViewImage
          source={product?.Image}
          alt={product?.Name}
          className="w-full h-28 md:w-72 md:h-56"
        />
      </div>

      <div className="px-2 md:px-5 py-1">
        <Link to={`/products/${product?.Slug}`}>
          <h5 className="text-md md:text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
            {product?.Name}
          </h5>
        </Link>

        <div className="md:flex items-end justify-between">
          <p className="text-sm md:text-lg truncate text-gray-700 dark:text-white">
            {`${product.Quantity} ${product?.Unit} at Rs. ${product?.Price}/${product?.Unit}`}
          </p>

          <Link
            to={`/products/${product?.Slug}`}
            className="text-white block bg-blue-600 hover:bg-blue-700  font-medium rounded-lg text-sm py-1 md:p-2 text-center dark:bg-blue-500 dark:hover:bg-blue-600"
          >
            Details
          </Link>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
