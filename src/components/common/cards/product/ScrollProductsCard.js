import React, { useRef } from "react";
import ProductCard from "./ProductCard";
import Arrow from "../../Arrow";

const ScrollProductsCard = ({ products, inWidth }) => {
  const ref = useRef();
  return (
    <>
      <Arrow
        handleClick={() => (ref.current.scrollLeft -= inWidth)}
        arrow="right"
      />
      <div
        ref={ref}
        className="max-w-screen-xl m-auto flex overflow-x-scroll scroll-smooth no-scrollbar"
      >
        {products.length > 0 &&
          products?.map((product, idx) => (
            <ProductCard key={idx} product={product} />
          ))}
      </div>
      <Arrow
        handleClick={() => (ref.current.scrollLeft += inWidth)}
        arrow="left"
      />
    </>
  );
};

export default ScrollProductsCard;
