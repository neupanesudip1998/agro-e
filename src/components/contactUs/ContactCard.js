import React from "react";

const ContactCard = ({ label, link, value, icon }) => {
  return (
    <div className="mb-2 flex w-full max-w-[370px]">
      <div className="bg-primary text-primary mr-6 flex h-[60px] w-full max-w-[60px] items-center justify-center overflow-hidden rounded bg-opacity-5 sm:h-[70px] sm:max-w-[70px]">
        <a href="tel:+97701-4423834">{icon}</a>
      </div>
      <div className="w-full">
        <h4 className="text-dark mb-1 text-xl font-bold">{label}</h4>
        <p className="text-blue-500 text-base">
          <a href={link}>{value}</a>
        </p>
      </div>
    </div>
  );
};

export default ContactCard;
