import React from "react";
import ContactCard from "./ContactCard";
import { LocationIcon, PhoneIcon, ViberIcon, WhatAppIcon } from "./Icons";

const ContactUs = () => {
  const number = "123456789";
  return (
    <section className="relative z-10 overflow-hidden bg-white  p-2 lg:py-10">
      <div className="container mx-auto">
        <div className="-mx-4 flex flex-wrap lg:justify-between">
          <div className="w-full px-4 lg:w-1/2 xl:w-6/12">
            <div className="mb-12 max-w-[570px] lg:mb-0">
              <span className="text-primary mb-4 block text-base font-semibold">
                Contact Us
              </span>
              <h2 className="text-dark mb-6 text-[32px] font-bold uppercase sm:text-[40px] lg:text-[36px] xl:text-[40px]">
                GET IN TOUCH WITH US
              </h2>
              <p className="text-body-color mb-9 text-base leading-relaxed">
                Top Nepal is a digital web media agency based in the historical
                city of Kathmandu. With collaboration from different web related
                organizations from the countries of US, Portugal, and Thailand,
                we are the hardworking, diligent and passionate team of 70+
                individuals. Our team includes IT professionals accompanied by
                Website Developers, Software Developer, Content writers, and
                marketing architects.
              </p>

              <ContactCard
                label="Our Location"
                value="Baluwatar, Pabitra workshop, Kathmandu, Nepal"
                icon={<LocationIcon />}
                link="https://www.google.com/maps/place/TOP+Nepal+IT+Solution/@27.7256609,85.3463511,18.08z/data=!4m6!3m5!1s0x39eb190852806153:0x8d04111c989d679c!8m2!3d27.7255783!4d85.3474655!16s%2Fg%2F11frnnqj1z?entry=ttu"
              />
              <ContactCard
                label="Phone Number"
                value="+977 01-4423834"
                icon={<PhoneIcon />}
                link="tel:+97701-4423834"
              />
              <ContactCard
                label="Viber Us"
                value={number}
                icon={<ViberIcon />}
                link={`viber://chat?number=%2B977-${number}`}
              />
              <ContactCard
                label="WhatsApp Us"
                value={number}
                icon={<WhatAppIcon />}
                link={`https://wa.me/+977${number}`}
              />
            </div>
          </div>
          <div className="w-full lg:w-1/2 xl:w-5/12">
            <div className="rounded-lg bg-gray-50 p-4 shadow-md sm:p-12">
              <form action="#" className="space-y-4">
                <div>
                  <label
                    htmlFor="email"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                  >
                    Your email
                  </label>
                  <input
                    type="email"
                    id="email"
                    className="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                    placeholder="example@gmail.com"
                    required
                  />
                </div>
                <div>
                  <label
                    htmlFor="subject"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                  >
                    Subject
                  </label>
                  <input
                    type="text"
                    id="subject"
                    className="block p-3 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 shadow-sm focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                    placeholder="Let us know how we can help you"
                    required
                  />
                </div>
                <div className="sm:col-span-2">
                  <label
                    htmlFor="message"
                    className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
                  >
                    Your message
                  </label>
                  <textarea
                    id="message"
                    rows="6"
                    className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                    placeholder="Leave a comment..."
                  ></textarea>
                </div>
                <button
                  type="submit"
                  className="py-3 px-5 text-sm font-medium text-center text-white rounded-lg bg-blue-700 sm:w-fit hover:bg-blue-800 focus:ring-4  focus:ring-blue-300"
                >
                  Send message
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ContactUs;
