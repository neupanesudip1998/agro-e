import React, { useEffect, useRef, useState } from "react";
import APIServices from "../../httpServices/httpServices";

import { Link } from "react-router-dom";
import Arrow from "../common/Arrow";
import Loading from "../common/Loading";
import SingleCategory from "../common/cards/category/SingleCategory";
import ScrollProductsCard from "../common/cards/product/ScrollProductsCard";
import { HOME_LIMIT, LIMIT } from "../../utils/constant";

export const LinkBar = ({ label, link }) => {
  return (
    <div className="flex items-center justify-between">
      <h1 className="text-2xl font-semibold mb-2 px-2">{label}</h1>
      <Link
        className="text-blue-500 text-lg hover:text-blue-600 font-semibold"
        to={link}
      >
        View All
      </Link>
    </div>
  );
};

const Home = () => {
  const ref3 = useRef(null);

  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);

  const [loadProduct, setLoadProduct] = useState(true);
  const [loadCategory, setLoadCategory] = useState(true);
  const [inWidth, setInWidth] = useState(0);

  const getCategories = async () => {
    const { data, success } = await new APIServices("category/list").post({
      limit: HOME_LIMIT,
    });

    if (success) {
      setCategories(data);
      setLoadCategory(false);
    }
  };

  const getProducts = async () => {
    const { data, success } = await new APIServices("product/list").post({
      limit: HOME_LIMIT,
    });

    if (success) {
      setProducts(data);
      setLoadProduct(false);
    }
  };

  useEffect(() => {
    getProducts();
    getCategories();
    const { innerWidth: width } = window;
    if (width > 1000) {
      setInWidth(400);
    } else {
      setInWidth(300);
    }
  }, []);

  return (
    <div>
      <LinkBar label="Categories" link="/categories" />
      <div className="relative">
        {loadCategory ? (
          <Loading />
        ) : (
          <>
            <Arrow
              handleClick={() => (ref3.current.scrollLeft -= inWidth)}
              arrow="right"
            />

            <div
              ref={ref3}
              className="max-w-screen-xl m-auto flex overflow-x-scroll scroll-smooth no-scrollbar"
            >
              {categories.length > 0 &&
                categories?.map((category, idx) => (
                  <SingleCategory key={idx} category={category} />
                ))}
            </div>
            <Arrow
              handleClick={() => (ref3.current.scrollLeft += inWidth)}
              arrow="left"
            />
          </>
        )}
      </div>

      <LinkBar label="Popular Products" link="/products/catalog/popular" />
      <div className="relative">
        {loadProduct ? (
          <Loading />
        ) : (
          <ScrollProductsCard inWidth={inWidth} products={products} />
        )}
      </div>

      <LinkBar label="Latest Products" link="/products/catalog/latest" />
      <div className="relative">
        {loadProduct ? (
          <Loading />
        ) : (
          <ScrollProductsCard inWidth={inWidth} products={products} />
        )}
      </div>
    </div>
  );
};

export default Home;
