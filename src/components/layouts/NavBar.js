import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";

import Logo from "../../assets/images/logo192.png";
import SearchBar from "./SearchBar";

export const NavLink = ({ title, path }) => {
  const { pathname } = useLocation();

  return (
    <li>
      <Link
        to={path}
        className={`${
          pathname === path ? "text-blue-500" : "text-gray-900"
        }  hover:text-blue-600 block py-2 pl-3 pr-4 rounded max-md:hover:bg-blue-200   md:border-0 md:p-0 `}
      >
        {title}
      </Link>
    </li>
  );
};

const NavBar = () => {
  const [nav, setNav] = useState(false);
  return (
    <nav className="z-20 fixed top-0 rigth-0 w-full bg-white border-gray-200 dark:bg-gray-900">
      <div className="max-w-screen-xl flex  flex-wrap items-center justify-between mx-auto p-4">
        <Link to="/" className="flex items-center">
          <img src={Logo} className="h-8 mr-3" alt="ecommerce" />
          <span className="self-center hidden sm:block text-2xl font-semibold whitespace-nowrap dark:text-white">
            Ecommerce
          </span>
        </Link>

        <SearchBar />

        <button
          onClick={() => setNav((prev) => !prev)}
          onBlur={() => {
            const timeRef = setTimeout(() => {
              setNav(false);
              clearTimeout(timeRef);
            }, 300);
          }}
          data-collapse-toggle="navbar-default"
          type="button"
          className="inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600"
          aria-controls="navbar-default"
          aria-expanded="false"
        >
          <span className="sr-only">Open main menu</span>
          <svg
            className="w-6 h-6"
            aria-hidden="true"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z"
              clipRule="evenodd"
            ></path>
          </svg>
        </button>
        <div
          className={`${!nav && "hidden"} w-full md:block md:w-auto`}
          id="navbar-default"
        >
          <ul className="font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
            <NavLink path="/" title="Home" />
            <NavLink path="/products" title="Product" />
            <NavLink path="/about" title="About" />
            <NavLink path="/contact" title="Contact" />
            <NavLink path="/profile" title="Profile" />
          </ul>
        </div>
      </div>
    </nav>
  );
};

export default NavBar;
