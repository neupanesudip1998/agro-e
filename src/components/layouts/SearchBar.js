import React, { useState } from "react";

import { Link, useNavigate } from "react-router-dom";

const SearchBar = () => {
  const navigate = useNavigate();
  const [search, setSearch] = useState("");
  const [suggest, setSuggest] = useState([]);
  const [model, setModel] = useState(false);
  const localSearch = JSON.parse(localStorage.getItem("search"));

  const handleChange = (e) => {
    const { value } = e.target;
    setSearch(value);
    const filter = localSearch
      ?.filter((item) => item.toLowerCase().indexOf(value.toLowerCase()) >= 0)
      .slice(0, 5);
    setSuggest(filter);
  };

  const handleRemove = (item) => {
    const filter = localSearch?.filter((elm) => elm !== item);
    localStorage.setItem("search", JSON.stringify(filter?.reverse()));
  };

  const handleClick = () => {
    const searchTxt = search.toString().trim();
    if (searchTxt) {
      let searchHistory = [];
      if (!localSearch) {
        searchHistory.push(searchTxt);
      } else {
        searchHistory = [...localSearch, searchTxt];
      }
      localStorage.setItem("search", JSON.stringify(searchHistory?.reverse()));
      navigate(`/products/search?q=${searchTxt}`);
    }
  };

  return (
    <div className="relative w-1/2 sm:w-1/3">
      <input
        onFocus={() => {
          setSuggest(localSearch?.slice(-5));
          setModel(true);
        }}
        onBlur={() => {
          const timeRef = setTimeout(() => {
            setModel(false);
            clearTimeout(timeRef);
          }, 300);
        }}
        type="search"
        className="block p-2.5 w-full z-20 text-sm text-gray-900 bg-gray-50 rounded-lg border-l-2 border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-l-gray-700  dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:border-blue-500"
        placeholder="Search products"
        onKeyUp={(e) => {
          if (e.key === "Enter") {
            handleClick();
            e.target.blur();
          }
        }}
        value={search}
        onChange={(e) => handleChange(e)}
      />
      <ul className={`${!model && "hidden"}  absolute z-10 w-full border-t-lg`}>
        {suggest &&
          suggest?.map((item, idx) => (
            <li
              key={idx}
              className="bg-green-100 shadow border-y-[1px] w-full p-2 flex items-center justify-between text-sm"
            >
              <Link
                onClick={() => setSearch(item)}
                className="hover:text-blue-600 hover:underline"
                to={`/products/search?q=${item}`}
              >
                {item}
              </Link>
              <span
                onClick={() => handleRemove(item)}
                className="text-red-500 cursor-pointer hover:underline"
              >
                Remove
              </span>
            </li>
          ))}
      </ul>
      <button
        onClick={handleClick}
        className="absolute top-0 right-0 p-2.5 text-sm font-medium text-white bg-blue-700 rounded-r-lg border border-blue-700 hover:bg-blue-800 focus:ring-2 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
      >
        <svg
          aria-hidden="true"
          className="w-5 h-5"
          fill="none"
          stroke="currentColor"
          viewBox="0 0 24 24"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          ></path>
        </svg>
        <span className="sr-only">Search</span>
      </button>
    </div>
  );
};

export default SearchBar;
