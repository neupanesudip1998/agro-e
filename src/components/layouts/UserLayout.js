import React, { useEffect } from "react";
import { Outlet } from "react-router-dom";
import NavBar from "./NavBar";
import Footer from "./Footer";

const UserLayout = () => {
  useEffect(() => {
    document.documentElement.classList.remove("dark");
    //document.documentElement.classList.add("dark");
  }, []);
  return (
    <div>
      <NavBar />
      <div className="max-w-screen-xl mx-auto p-2 md:p-4 mt-20">
        <Outlet />
      </div>

      <Footer />
    </div>
  );
};

export default UserLayout;
