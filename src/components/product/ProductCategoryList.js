import React from "react";
import { useParams } from "react-router-dom";

const ProductCategoryList = () => {
  const { category } = useParams();

  console.log(category);
  return <div> {category} Product</div>;
};

export default ProductCategoryList;
