import React, { useEffect, useState } from "react";
import { LIMIT } from "../../utils/constant";
import APIServices from "../../httpServices/httpServices";
import Loading from "../common/Loading";
import ProductCard from "../common/cards/product/ProductCard";
import Pagination from "../common/Pagination";
import { useLocation } from "react-router-dom";

const ProductList = () => {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  const { search } = useLocation();

  const page = Number(search?.split("page=")[1]) || 1;

  const [totalPage, setTotalPage] = useState(0);

  const getProducts = async () => {
    const { data, success, totalPages } = await new APIServices(
      "product/list"
    ).post({
      limit: LIMIT,
      page: page,
    });

    if (success) {
      setProducts(data);
      setTotalPage(totalPages);
      setLoading(false);
    }
  };

  useEffect(() => {
    setLoading(true);
    getProducts();
  }, [page]);

  return (
    <>
      {loading ? (
        <div className="h-96 flex items-center bg-slate-100">
          <div className="relative left-1/2">
            <Loading />
          </div>
        </div>
      ) : (
        <div>
          <div className="p-0 flex flex-wrap max-[500px]:justify-between">
            {products.length > 0 &&
              products.map((product, idx) => (
                <div key={idx} className="mx-1 md:mx-0">
                  <ProductCard product={product} />
                </div>
              ))}
          </div>
          <Pagination currentPage={page} totalPagesNumber={totalPage} />
        </div>
      )}
    </>
  );
};

export default ProductList;
