import React, { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import APIServices from "../../httpServices/httpServices";
import Loading from "../common/Loading";
import ProductCard from "../common/cards/product/ProductCard";
import Pagination from "../common/Pagination";
import { LIMIT } from "../../utils/constant";
import Filter from "../common/Filter";

const SearchedProducts = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const searchTxt = searchParams.get("q");
  const page = Number(searchParams.get("page")) || 1;

  const [totalPage, setTotalPage] = useState(0);

  const [products, setProducts] = useState([]);
  const [categories, setCategories] = useState([]);

  const [loading, setLoading] = useState(true);

  const initial = { Categories: [], OnSell: "", MinPrice: "", MaxPrice: "" };

  const [filterObj, setFilterObj] = useState(initial);

  const getProducts = async () => {
    const { data, success, totalPages } = await new APIServices(
      "product/list"
    ).post({
      page: page || 1,
      limit: LIMIT,
      search: {
        ...filterObj,
        search: searchTxt,
      },
    });

    if (success) {
      setTotalPage(totalPages);
      setProducts(data);
      setLoading(false);
    }
  };

  const getCategoryName = async () => {
    const { data, success } = await new APIServices("category/name").get();
    if (success) {
      setCategories(data);
    }
  };

  useEffect(() => {
    getCategoryName();
  }, []);

  useEffect(() => {
    setLoading(true);
    getProducts();
  }, [searchTxt, page]);

  return (
    <>
      {loading ? (
        <div className="h-96 flex items-center bg-slate-100">
          <div className="relative left-1/2">
            <Loading />
          </div>
        </div>
      ) : (
        <div className="md:flex">
          <Filter
            setLoading={setLoading}
            getProducts={getProducts}
            categories={categories}
            filterObj={filterObj}
            setFilterObj={setFilterObj}
            initial={initial}
          />
          <div>
            <div className="p-0 flex flex-wrap max-[500px]:justify-between">
              {products.length > 0 &&
                products.map((product, idx) => (
                  <div key={idx} className="mx-1 md:mx-0">
                    <ProductCard product={product} />
                  </div>
                ))}
            </div>
            <Pagination currentPage={page} totalPagesNumber={totalPage} />
          </div>
        </div>
      )}
    </>
  );
};

export default SearchedProducts;
