import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import DOMPurify from "dompurify";
import APIServices from "../../httpServices/httpServices";
import ViewImage from "../common/ViewImage";
import DataView from "../common/DataView";
import Loading from "../common/Loading";

const ViewProduct = () => {
  const { slug } = useParams();

  const [product, setProduct] = useState("");
  const [current, setCurrent] = useState(0);
  const [loading, setLoading] = useState(true);

  const getProduct = async () => {
    const { data, success } = await new APIServices(`product/${slug}`).get();

    if (success) {
      const newP = { ...data, Gallary: [data?.Image, ...data?.Gallary] };
      setProduct(newP);
      setLoading(false);
    }
  };

  useEffect(() => {
    getProduct();
  }, [slug]);

  return (
    <>
      {loading ? (
        <div className="h-96 flex items-center bg-slate-100">
          <div className="relative left-1/2">
            <Loading />
          </div>
        </div>
      ) : (
        <div className="md:flex items-start justify-center">
          {/* Image panal */}
          <div>
            <ViewImage
              className={`max-[450px]:h-[250px] h-[350px] sm:w-[750px] lg:h-[450px]
         rounded-t-lg `}
              source={product?.Gallary ? product?.Gallary[current] : ""}
              alt={product?.Name}
            />

            <div className="flex justify-start mt-1">
              {product?.Gallary &&
                product?.Gallary.map((image, idx) => (
                  <div
                    key={idx}
                    onClick={() => setCurrent(idx)}
                    className="flex-1 border mr-1 max-w-[120px] max-h-[120px]"
                  >
                    <ViewImage
                      className={`w-full h-full rounded-b-lg  ${
                        idx === current && "border opacity-50 "
                      }`}
                      source={image}
                      alt={product?.Name}
                    />
                  </div>
                ))}
            </div>
          </div>

          {/* Details panal */}
          <div className="xl:w-2/5 md:w-1/2 lg:ml-8 md:ml-6 md:mt-0 mt-6">
            <div className="border-b border-gray-200 pb-6">
              <p className="text-sm leading-none text-gray-600">
                {`Category ${product?.Category?.Name}`}
              </p>
              <h1 className="lg:text-2xl text-xl font-semibold lg:leading-6 leading-7 text-gray-800 mt-2">
                {product?.Name}
              </h1>
            </div>
            <div className="py-4 border-b border-gray-200 flex items-center justify-between">
              <p className="text-base leading-4 text-gray-800">Farmer</p>
              <div className="flex items-center justify-center">
                <p className="text-sm leading-none text-gray-600">
                  {`${product?.Farmer?.FirstName} ${product?.Farmer?.LastName}`}
                </p>
              </div>
            </div>
            <div className="py-4 border-b border-gray-200 flex items-center justify-between">
              <p className="text-base leading-4 text-gray-800">Contact</p>
              <div className="flex items-center justify-center">
                <p className="text-sm leading-none text-gray-600 mr-3">
                  <a href={`tel:+977${product?.Farmer?.Phone}`}>
                    {product?.Farmer?.Phone}
                  </a>
                </p>
              </div>
            </div>
            <p
              className={`text-base flex items-center	justify-center	leading-none	text-white ${
                product?.OnSell === "Yes" ? "bg-green-600" : "bg-red-500"
              }	w-full py-4`}
            >
              Product {product?.OnSell === "Yes" ? "On sell" : "Not for sale"}
            </p>
            <div>
              <div
                className=" text-base lg:leading-tight leading-normal text-gray-600 mt-7"
                dangerouslySetInnerHTML={{
                  __html: DOMPurify.sanitize(product?.Detail),
                }}
              />

              <DataView label="Product Name" value={product?.Name} />
              <DataView
                label="Product Quantity"
                value={` ${product?.Quantity} ${product?.Unit}`}
              />
              <DataView
                label="Product Price: NRP"
                value={` ${product?.Price}/${product?.Unit}`}
              />
              <DataView
                label="Product Published On:"
                value={` ${new Date(product?.PublishedOn)?.toDateString()}`}
              />
              <DataView
                label="Product Expired Date:"
                value={` ${product?.ExpiredDate}`}
              />
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default ViewProduct;
