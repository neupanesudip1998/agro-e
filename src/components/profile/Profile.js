import React, { useEffect, useRef, useState } from "react";
import APIServices from "../../httpServices/httpServices";
import DataView from "../common/DataView";
import ViewImage from "../common/ViewImage";
import { LIMIT } from "../../utils/constant";
import ProductCard from "../common/cards/product/ProductCard";
import Pagination from "../common/Pagination";

const Profile = () => {
  const ref = useRef(null);
  const [profile, setProfile] = useState("");
  const [products, setProducts] = useState([]);
  const [page, setPage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);

  const getProfile = async () => {
    const { success, data, message } = await new APIServices(
      "user/my-details"
    ).get();

    if (success) {
      setProfile(data);
    } else {
      console.log(message);
    }
  };

  const getFarmerProduct = async () => {
    const { data, success, message, totalPages } = await new APIServices(
      "/product/my"
    ).post({});

    if (success) {
      setProducts(data);
      setTotalPage(totalPages);
    } else {
      console.log(message);
    }
  };

  const handleScroll = () => {
    let view = window.innerHeight + window.scrollY;
    let viewPoint = ref?.current.offsetTop;
    if (view >= viewPoint) {
      window.removeEventListener("scroll", handleScroll);
      getFarmerProduct();
    }
  };

  useEffect(() => {
    getProfile();
    window.addEventListener("scroll", handleScroll, {
      passive: true,
    });
  }, []);

  return (
    <div className="max-w-6xl px-4 py-4 mx-auto lg:py-8 md:px-6">
      <div>
        <div className="flex gap-x-4">
          <div className="user-profile w-[180px] h-[180px]">
            <ViewImage
              className="employee-avatar object-cover h-full w-full rounded-md"
              source={!!profile?.ProfileImage ? profile?.ProfileImage : ""}
            />
          </div>
          <div className="flex-1">
            <div className="flex items-center justify-between">
              <div>
                <div className="font-bold text-2xl" name="">
                  {profile?.FirstName} {profile?.MiddleName} {profile?.LastName}
                </div>

                <div className="mb-3 text-gray-500">
                  <span className="text-base">{profile?.Email}</span>
                </div>
              </div>
            </div>

            <div className="grid grid-cols-3 gap-x-2">
              <div className="flex flex-col">
                <span className="block mb-0 text-sm text-gray-600 font-normal">
                  Date of Birth
                </span>
                <span className="font-bold">
                  {new Date(profile?.DOB)?.toDateString()}
                </span>
              </div>

              <div className="flex flex-col">
                <span className="block mb-0 text-sm text-gray-600 font-normal">
                  Phone
                </span>
                <span className="mb-5 text-base font-semibold text-gray-800">
                  {profile?.Phone ? profile?.Phone : "--"}{" "}
                </span>
              </div>

              <div className="flex flex-col">
                <span className="block mb-0 text-sm text-gray-600 font-normal">
                  Cellphone
                </span>
                <span className="mb-5 text-base font-semibold text-gray-800">
                  {profile?.Mobile ? profile?.Mobile : "--"}
                </span>
              </div>
            </div>

            <div className="flex items-center justify-between">
              <div className="flex items-center">
                <div
                  className={`rounded-full w-3 h-3 bg-green-700 mr-2 ${
                    profile?.Status === "Active" ? "bg-green-700" : "bg-red-700"
                  }`}
                ></div>
                <span className="text-sm"> {profile?.Status} </span>
              </div>
            </div>
          </div>
        </div>

        <div className="shadow bg-white border-gray-500 p-8 rounded mt-14">
          <h2 className="font-bold text-lg mt-5 mb-2">Farmer's Information</h2>
          <div className="grid grid-cols-2">
            <DataView
              label="Farmer Identity No"
              value={profile?.FarmerIdentityNo}
            />
            <DataView
              label="National Farmer No"
              value={profile?.NationalFarmerNo}
            />
            <DataView
              label="Devnagari First Name"
              value={profile?.DevnagariFirstName}
            />
            <DataView
              label="Devnagari Middle Name"
              value={profile?.DevnagariMiddleName}
            />
            <DataView
              label="Devnagari Last Name"
              value={profile?.DevnagariLastName}
            />
            <DataView label="Gender" value={profile?.Gender} />
            <DataView
              label="Citizenship Number"
              value={profile?.CitizenshipNumber}
            />
            <DataView
              label="Citizenship District"
              value={profile?.CitizenshipDistrict}
            />
            <DataView
              label="Citizenship Issue Date"
              value={profile?.CitizenshipIssueDate}
            />
            <DataView label="National Id No" value={profile?.NationalId} />
            <DataView label="Country" value={profile?.Country} />

            <DataView label="Province" value={profile?.Province} />
            <DataView label="District" value={profile?.District} />
            <DataView label="Local Bodies" value={profile?.LocalBodies} />
            <DataView label="Ward Number" value={profile?.WardNo} />
            <DataView label="Tole" value={profile?.Tole} />
            <DataView label="Address" value={profile?.Address} />
            <DataView label="Latitude" value={profile?.Latitude} />
            <DataView label="Longitude" value={profile?.Longitude} />
            <DataView label="Education" value={profile?.Education} />
          </div>
        </div>
      </div>
      <div ref={ref}>
        <h3>List of product</h3>

        <>
          {
            <div>
              <div className="p-0 flex flex-wrap max-[500px]:justify-between">
                {products.length > 0 &&
                  products.map((product, idx) => (
                    <div key={idx} className="mx-1 md:mx-0">
                      <ProductCard product={product} />
                    </div>
                  ))}
              </div>
              {/* <Pagination currentPage={page} totalPagesNumber={totalPage} /> */}
            </div>
          }
        </>
      </div>
    </div>
  );
};

export default Profile;
